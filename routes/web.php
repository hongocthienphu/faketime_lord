<?php

use App\Http\Controllers\ChatController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [CustomerController::class, 'index']);
Route::post('/dang-ky', [CustomerController::class, 'dangKy']);
Route::post('/dang-nhap', [CustomerController::class, 'dangNhap']);
Route::get('dang-xuat', [CustomerController::class, 'dangXuat']);
Route::get('/chat', [ChatController::class, 'index']);
Route::get('/chat/data', [ChatController::class, 'getData']);
Route::post('/chat/send-message', [ChatController::class, 'sendMessage']);
