<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $check = Auth::guard('customer')->check();
        if ($check) {
            $customer_id = Auth::guard('customer')->id();
            return view('chat', compact('customer_id'));
        } else {
            return redirect('/');
        }
    }
    public function getData(){
        $data = Chat::all();
        return response()->json(['data' => $data]);
    }

    public function sendMessage(Request $request){
        $check = Auth::guard('customer')->check();
        if ($check) {
            $customer_id = Auth::guard('customer')->id();
            Chat::create([
                'content' => $request->content,
                'customer_id' => $customer_id,
            ]);
        } else {
            return redirect('/');
        }
    }

}
