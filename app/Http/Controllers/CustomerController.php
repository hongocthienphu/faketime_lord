<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth');
    }
    public function dangKy(Request $request)
    {
        Customer::create([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        return redirect('/');
    }

    public function dangNhap(Request $request)
    {
        $data = $request->only('email', 'password');
        $check = auth('customer')->attempt($data);
        if($check){
            return redirect('/chat');
        } else {
            return redirect('/');
        }
    }

    public function dangXuat(){
        Auth::guard('customer')->logout();
        return redirect('/');
    }
}
