<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Đăng Ký Tài Khoản
                    </div>
                    <div class="card-body">
                        <form action="/dang-ky" method="post">
                            @csrf
                            <div class="form-group">
                              <label>Họ Và Tên</label>
                              <input name="full_name" type="text" class="form-control" placeholder="Nhập vào họ và tên">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Nhập vào email">
                            </div>
                            <div class="form-group">
                                <label>Mật Khẩu</label>
                                <input name="password" type="password" class="form-control" placeholder="Nhập vào mật khẩu">
                            </div>
                            <button type="submit" class="btn btn-danger">Đăng Ký</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Đăng Nhập Hệ Thống
                    </div>
                    <div class="card-body">
                        <form action="/dang-nhap" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Nhập vào email">
                            </div>
                            <div class="form-group">
                                <label>Mật Khẩu</label>
                                <input name="password" type="password" class="form-control" placeholder="Nhập vào mật khẩu">
                            </div>
                            <button type="submit" class="btn btn-primary">Đăng Nhập</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
  </body>
</html>
