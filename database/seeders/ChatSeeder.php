<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chats')->delete();

        DB::table('chats')->truncate();

        DB::table('chats')->insert([
            ['content' => 'Xin chào cả lớp', 'customer_id' => 1],
            ['content' => 'Cả lớp đi nhậu không?', 'customer_id' => 2],
            ['content' => 'Ok được đó, nhưng ai bao?', 'customer_id' => 1],
            ['content' => 'Tao bao', 'customer_id' => 3],
            ['content' => 'Mi là ai?', 'customer_id' => 2],
            ['content' => 'Tao là Sơn Công', 'customer_id' => 3],
            ['content' => 'Quá tuyệt vời bạn ơi', 'customer_id' => 1],
            ['content' => 'Đi qua Bé Biển nhậu thôi', 'customer_id' => 1],
            ['content' => 'Ok con dê', 'customer_id' => 2],
            ['content' => 'Ok con bò', 'customer_id' => 3],
        ]);
    }
}
